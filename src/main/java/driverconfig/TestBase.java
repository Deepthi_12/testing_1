package driverconfig;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.Properties;
import java.util.concurrent.TimeUnit;

import org.apache.commons.io.FileUtils;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.ITestListener;
import org.testng.ITestResult;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeSuite;
import org.testng.annotations.BeforeTest;

public class TestBase{
	
	public WebDriver driver;
	public Properties prop;
	
	
	
	
	@BeforeSuite
	public void loadPropertiesData() {
		
		prop = new Properties();
		FileInputStream fis;
		try {
			 fis = new FileInputStream(System.getProperty("user.dir")+"\\src\\main\\java\\properties\\config.properties");
				prop.load(fis);
		} catch (Exception e) {
		
			e.printStackTrace();
		}
	}
		
		
	@BeforeTest
	public void launchApp() {
		if(prop.getProperty("browser").equals("chrome")) {
		System.setProperty("webdriver.chrome.driver", System.getProperty("user.dir")+"\\Drivers\\chromedriver.exe");
		driver = new ChromeDriver();
		
		}
		else if (prop.getProperty("browser").equals("firefox")){
			
			
		}
		
		else {
			
			System.out.println("please specify the browser");
		}
		
		driver.get(prop.getProperty("appUrl"));
		driver.manage().window().maximize();
		driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
		
		
	}
	
	@AfterTest
	
	public void closeBrowser() {
		
		driver.quit();
	}
	
	
	
	@AfterMethod
	public void failScrennshot(ITestResult result) {
		if(ITestResult.FAILURE == result.getStatus()) {		
		TakesScreenshot ts  = ((TakesScreenshot)driver);
		File src = ts.getScreenshotAs(OutputType.FILE);
		try {
			FileUtils.copyFile(src, new File("C:\\Users\\Deepthi Chandra\\Desktop\\eclipse-workspace\\Testing_1\\screenshots\\"+result.getName()+".jpg"));
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		}
}
	

}
