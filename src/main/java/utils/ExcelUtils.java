package utils;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;

import org.apache.poi.xssf.usermodel.XSSFCell;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

public class ExcelUtils {
	
	public String path;
    FileInputStream fis;
    XSSFWorkbook workbook;
    XSSFSheet sheet;
    XSSFRow row;
    XSSFCell cell;

    public ExcelUtils(String path) {
        this.path = path;
        try {
            fis = new FileInputStream(path);
            workbook = new XSSFWorkbook(fis);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public ExcelUtils(){


    }

    public String getCellData(String sheetName, int colNum, int rowNum) {
        try {
            int index = workbook.getSheetIndex(sheetName);
            sheet = workbook.getSheetAt(index);
            //XSSFRow row = sheet.getRow(0);
            row = sheet.getRow(rowNum-1); //3=0 1 2 3
            cell = row.getCell(colNum);

            return cell.getStringCellValue();


        } catch (Exception e) {
            e.printStackTrace();
        }

        return null;
    }


    public int getRowCount(String sheetName) {
        try {
            int index = workbook.getSheetIndex(sheetName);
            if (index == -1) {
                return 0;
            } else {
                sheet = workbook.getSheetAt(index);
                int number = sheet.getPhysicalNumberOfRows();
                return number;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return 0;
    }

    public int getColumnCount(String sheetName) {
        try {
            int index = workbook.getSheetIndex(sheetName);
            if (index == -1) {
                return 0;
            } else {
                sheet = workbook.getSheet(sheetName);
                row = sheet.getRow(0);
                return row.getLastCellNum();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return 0;
    }


    public void putCellData(String filePath,int sheetNo,int RowNum, int ColNum, String Value) throws Exception{

        try{
            sheet = workbook.getSheetAt(sheetNo);
            cell = sheet.getRow(RowNum).getCell(ColNum);
            cell.setCellValue(Value);
            FileOutputStream out = new FileOutputStream(new File(filePath));
            workbook.write(out);
            out.flush();
            out.close();
        }catch (Exception e){
            System.out.println("Error in Putting cell data...");
            e.printStackTrace();
        }
    }

}