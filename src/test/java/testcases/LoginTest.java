package testcases;

import javax.naming.directory.SearchResult;

import org.testng.Assert;
import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import driverconfig.TestBase;
import driverconfig.TestListeners;
import pagelibrary.DashBoard_page;
import pagelibrary.LoginPage;

//@Listeners(TestBase.class)
public class LoginTest extends TestBase {
	
	LoginPage loginPage;
	DashBoard_page DashBoard_Page;
	   
	
	@Test(priority = 0)
	public void loginValidation() {
		
		Assert.assertEquals("Your store. Login", driver.getTitle());
		loginPage  = new LoginPage(driver);
		loginPage.Login_app(prop.getProperty("username"), prop.getProperty("pwd"));
		

		
	}
   @Test(priority = 1)
   
   public void dashBoardPageValidation() {
	   
	  
	   DashBoard_Page= new DashBoard_page(driver);
	   Assert.assertTrue(DashBoard_Page.IsDashBoardLabelDisplayed());
	   String output_Lable = DashBoard_Page.SearchField("LOW");
	   Assert.assertEquals("Low stocrtk", output_Lable);
	   
	   
   }
   
   @Test(priority= 2)
   
   public void Searchtabvalidation() {
	   
	//DashBoard_Page= new DashBoard_page(driver);
	 
	System.out.println(DashBoard_Page.Searchtab());
	Assert.assertTrue(false);
   }
   
   
   

}
