package pagelibrary;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.Select;

public class DashBoard_page {
	WebDriver driver;
   @FindBy(xpath = "//div[@class='content-header']/h1")
   WebElement dashBoardLabel;

   @FindBy(xpath = "//input[@placeholder='Search']")
   WebElement SearchField;
   
   @FindBy(id = "user-selection")
   WebElement Searchselection;
  
   
   @FindBy(xpath = "//div[@class='content-header clearfix']/h1")
   WebElement Search_Lable;
   
   
   
   @FindBy(id = "SearchPublishedId")
   WebElement Search_tab;

   
   
   
   
   
	public DashBoard_page(WebDriver driver) {
		this.driver = driver;
		PageFactory.initElements(driver, this);
		
	}

 
   

public boolean IsDashBoardLabelDisplayed() {
	return dashBoardLabel.isDisplayed();
}


public String SearchField(String searchname) {
	SearchField.sendKeys(searchname);
	Searchselection.click();
	return Search_Lable.getText();
	
}

public String Searchtab() {
     Select dropdown =new Select(Search_tab);
     dropdown.selectByIndex(2);
     return dropdown.getFirstSelectedOption().getText();

}
}
