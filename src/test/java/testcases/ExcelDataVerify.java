package testcases;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.testng.Assert;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import driverconfig.TestBase;
import utils.ExcelUtils;

public class ExcelDataVerify extends TestBase {
	
	String filePath  = "C:\\Users\\Deepthi Chandra\\Desktop\\eclipse-workspace\\Testing_1\\src\\main\\java\\testdata\\Book1.xlsx";
	
	
	
	
	//@Test
	public void readData() {
		
		ExcelUtils e = new ExcelUtils(filePath);
		System.out.println(e.getRowCount("Sheet1"));
		System.out.println(e.getColumnCount("Sheet1"));
		System.out.println(e.getCellData("Sheet1", 1, 3));// takes row & column numbers

		
	}



public Object[][] getData(String SheetName) {
    ExcelUtils Data = new ExcelUtils(filePath);
    int rowNum = Data.getRowCount(SheetName)-1; // 3 0  1 2 
    System.out.println(rowNum);
    int colNum = Data.getColumnCount(SheetName);
    System.out.println(colNum);
    Object sampleData[][] = new Object[rowNum][colNum];
    for (int i = 2; i <=rowNum+1; i++) {
        for (int j = 0; j <colNum; j++) {
            sampleData[i - 2][j] = Data.getCellData(SheetName, j, i);
        }
    }
    return sampleData;
}

@DataProvider(name="datas")
public Object[][] getExcelData(){
    Object[][] data = getData( "Sheet1");
    return data;

}

//@Test(dataProvider = "datas")
public void m(String uname, String pwd) throws Exception{
	WebElement email = driver.findElement(By.id("Email"));
	email.clear();
	email.sendKeys(uname);
	WebElement password = driver.findElement(By.id("Password"));
	password.clear();
	password.sendKeys(pwd);
  WebElement ele = driver.findElement(By.xpath("//input[@type='submit']"));
  ele.click();
  //Java_scriptExecutor is used to perform click, sendkeys & frame selections options , for latest webbrowsers which are developed by angular js or 
  //some other latest technologies click & some other functions cannot be performed by using selinium, so we are using javascriptexceutor for that
  // JavascriptExecutor js  = (JavascriptExecutor)driver;
  // js.executeScript("arguments[0].click()", ele);
   if(uname.equals("admin@yourstore.com") && pwd.equals("admin")){
	   
	   
	   Assert.assertTrue(driver.findElement(By.xpath("//div[@class='content-header']/h1")).isDisplayed());
   }
   else {
	   Assert.assertEquals("Wrong email", driver.findElement(By.xpath("//span[@id='Email-error']")).getText());
	   
   }


}
// @Test
public void putData() throws Exception{

    ExcelUtils excelUtils = new ExcelUtils();
    excelUtils.putCellData("C:\\Users\\Sai Ram\\Desktop\\data.xlsx",0,3,4,"ayo");
}}
